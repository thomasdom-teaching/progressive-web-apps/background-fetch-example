const staticDevCoffee = "dev-coffee-site-v1";
const assets = [
  "/",
  "/index.html",
  "/css/style.css",
  "/js/app.js",
];

self.addEventListener("install", installEvent => {
  installEvent.waitUntil(
    caches.open(staticDevCoffee).then(cache => {
      cache.addAll(assets);
    })
  );
});

// On success, store video in cache
self.addEventListener('backgroundfetchsuccess', (event) => {
  const backgroundFetch = event.registration;

  console.log("Success! Downloaded assets:", backgroundFetch.matchAll());

  event.waitUntil(async () => {
    const cache = await caches.open('video-cache');
    const records = await backgroundFetch.matchAll();

    const promises = records.map(async record => {
      const response = await record.responseReady;
      await cache.put(record.request, response);
    });

    await Promise.all(promises);

    event.updateUI({ title: 'Video successfully downloaded!' });
  });
});

// On failure, display an error
self.addEventListener('backgroundfetchfail', (event) => {
  console.error('Could not download video:', event);

  event.waitUntil(async () => {
    event.updateUI({ title: 'Could not download video.' });
  });
});

// On abort from user, display an error
self.addEventListener('backgroundfetchabort', event => {
  console.log('[Service Worker]: Background Fetch Abort', event.registration);
  console.error('Aborted by the user. No data was saved.');
});

// On click, open the website to play the downloaded media
addEventListener('backgroundfetchclick', event => {
  console.log('[Service Worker]: Background Fetch Click', event.registration);
  event.waitUntil(async () => {
    clients.openWindow('/');
  });
});
