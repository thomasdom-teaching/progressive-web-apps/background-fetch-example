const downloadButton = document.getElementById('download-in-background');

downloadButton.onclick = () => {
  // Add background fetch code here
  // Bad video URL : http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4
  // Good video URL : /videos/BigBuckBunny.mp4
  if ('BackgroundFetchManager' in self) {
    navigator.serviceWorker.ready
      .then(async registration => {
        const backgroundFetch = await registration.backgroundFetch.fetch(
          'my-video',
          '/videos/BigBuckBunny.mp4',
          {
            title: 'Saving "Big Buck Bunny"...',
            downloadTotal: 158008374
          }
        );

        // Follow download progress in app 
        backgroundFetch.addEventListener('progress', () => {
          // If we didn't provide a total, we can't provide a %.
          if (!backgroundFetch.downloadTotal) return;

          const percent = Math.round(backgroundFetch.downloaded / backgroundFetch.downloadTotal * 100);
          console.log(`Download progress: ${percent}%`);
        });
      });
  }
  else {
    console.error('Browser does not support background fetch.');
  }
}

if ("serviceWorker" in navigator) {
  window.addEventListener("load", function () {
    navigator.serviceWorker
      .register("/serviceWorker.js")
      .then(res => console.log("service worker registered"))
      .catch(err => console.log("service worker not registered", err));
  });
}
